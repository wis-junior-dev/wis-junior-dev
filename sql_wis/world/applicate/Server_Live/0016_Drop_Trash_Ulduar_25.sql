-- entry trash 25 man 33729,33737,33732,33731,33735,33734,33741,33733,33700,33723,33757,33758,33773,33827,33829,33830,33828,33832,33831,34115,34185,34186,34201,34139,34141,34142,34140,34214,34229,34220,34245,34226,34236,34237,34268,34270,34272,34274

DELETE FROM `reference_loot_template` WHERE `entry` = '34156';
INSERT INTO `reference_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES
('34156','1','40','1','0','-34106','1'),
('34156','2','40','1','0','-34106','1'),
('34156','3','30','1','0','-34107','1'),
('34156','4','15','1','0','-34108','1'),
('34156','5','20','1','0','-34109','1'),
('34156','6','5','1','0','-34110','1'),
('34156','7','2.5','1','0','-34155','1');

DELETE FROM `reference_loot_template` WHERE `entry` = '34155';
INSERT INTO `reference_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES
('34155','45538','0','1','1','1','1'), -- Titanstone Pendant
('34155','45539','0','1','1','1','1'), -- Pendant of Focused Energies
('34155','45540','0','1','1','1','1'), -- Bladebearer's Signet
('34155','45541','0','1','1','1','1'), -- Shroud of Alteration
('34155','45542','0','1','1','1','1'), -- Greaves of the Stonewarder
('34155','45543','0','1','1','1','1'), -- Shoulders of Misfortune
('34155','45544','0','1','1','1','1'), -- Leggings of the Tortured Earth
('34155','45547','0','1','1','1','1'), -- Relic Hunter's Cord
('34155','45548','0','1','1','1','1'), -- Belt of the Sleeper
('34155','45549','0','1','1','1','1'), -- Grips of Chaos
('34155','45605','0','1','1','1','1'), -- Daschal's Bite
('34155','46138','0','1','1','1','1'); -- Idol of the Flourishing Life

DELETE FROM `creature_loot_template` WHERE `entry` = '33733';
INSERT INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES
('33733','1','100','1','0','-34156','1'),
('33733','20','0.1','1','0','-600000','1'),
('33733','45912','0.05','1','0','1','1');

UPDATE `creature_template` SET `lootid` = '33773' WHERE `entry` = '33773';
DELETE FROM `creature_loot_template` WHERE `entry` = '33773';
INSERT INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES
('33773','1','100','1','0','-34156','1'),
('33773','20','0.1','1','0','-600000','1'),
('33773','45912','0.05','1','0','1','1');