-- smart_scripts NPC 24824
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`='24824';
DELETE FROM `smart_scripts` WHERE `entryorguid` IN ('24824', '2482400');
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
('24824','0','0','0','8','0','100','0','44610','0','0','0','80','2482400','2','0','0','0','0','1','0','0','0','0','0','0','0','Relic - SpellHit - Action List'),
('2482400','9','0','0','0','0','100','0','5000','5000','0','0','33','24824','0','0','0','0','0','7','0','0','0','0','0','0','0','Relic - Credit'),
('2482400','9','1','0','0','0','100','0','0','0','0','0','41','0','0','0','0','0','0','1','0','0','0','0','0','0','0','Relic - Despawn');

-- update creature_template NPC 24825
UPDATE `creature_template` SET `spell1`='44608', `spell2`='44610', `spell3`='44609',`AIName`='SmartAI', `InhabitType`='1' WHERE `entry`='24825';

-- conditions
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`='13' AND `SourceEntry`='44610';
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
('13','1','44610','0','31','3','24824','0','0','','Collect Data target');