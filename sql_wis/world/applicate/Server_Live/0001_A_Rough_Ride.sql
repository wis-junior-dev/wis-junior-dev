-- NPC entry 28298 (Captive Crocolisk)
UPDATE `creature_template` SET `gossip_menu_id`='9674', `npcflag`=npcflag|1, `VehicleId`='0' WHERE `entry`='28298';
DELETE FROM `gossip_menu` WHERE `entry`='9674';
INSERT INTO `gossip_menu` (`entry`, `text_id`) VALUES
('9674', '13120');

-- Il gossip � visibile solo se si ha la quest 12536
DELETE FROM `conditions` WHERE `SourceGroup`='9674' AND `SourceTypeOrReferenceId`='15';
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
('15', '9674', '0', '0', '9', '12536', '0', '0', '0', '0', 'Only show gossip 9674 when quest 12536 is added');

-- Gossip 9674
DELETE FROM `gossip_menu_option` WHERE `menu_id`='9674' AND `id`='0';
INSERT INTO `gossip_menu_option` (`menu_id`, `id`, `option_icon`, `option_text`, `option_id`, `npc_option_npcflag`, `action_menu_id`, `action_poi_id`, `box_coded`, `box_money`, `box_text`) VALUES 
('9674', '0', '0', "You look safe enough... let's do this.", '1', '1', '0', '0', '0', '0', '');

-- Smart_scripts NPC 28298
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry` ='28298';
DELETE FROM `smart_scripts` WHERE `entryorguid` ='28298';
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`)
VALUES
('28298','0','0','1','62','0','100','0','9674','0','0','0','85','51258','0','0','0','0','0','7','0','0','0','0','0','0','0','Captive Crocolisk - On Gossip Option 0 Selected - Cast \'Forcecast Summon Crocolisk\''),
('28298','0','1','0','61','0','100','512','0','0','0','0','45','1','3','0','0','0','0','19','28216','0','0','0','0','0','0','Captive Crocolisk - On Gossip Option 0 Selected - Set Data on Zepik');

-- Smart_scripts NPC 28308
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry` ='28308';
DELETE FROM `smart_scripts` WHERE `entryorguid` IN ('28308', '2830800');
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`)
VALUES
('28308','0','0','0','54','0','100','0','0','0','0','0','80','2830800','0','2','0','0','0','1','0','0','0','0','0','0','0','Captive Crocolisk - On Just Summoned - Run Script (Phase 1) (No Repeat)'),
('28308','0','1','2','40','0','100','0','52','0','0','0','11','50630','2','0','0','0','0','1','0','0','0','0','0','0','0','Captive Crocolisk - On reached WP51 - Cast Eject All Passengers'),
('28308','0','2','3','61','0','100','0','0','0','0','0','15','12536','0','0','0','0','0','7','0','0','0','0','0','0','0','Captive Crocolisk - On reached WP51 - Quest Credit \'A Rough Ride\''),
('28308','0','3','4','61','0','100','0','0','0','0','0','85','52545','2','0','0','0','0','7','0','0','0','0','0','0','0','Captive Crocolisk - On reached WP51 - Cast Forceitem Zepik'),
('28308','0','4','0','61','0','100','0','0','0','0','0','41','0','0','0','0','0','0','1','0','0','0','0','0','0','0','Captive Crocolisk - On reached WP52 - Despawn'),
('2830800','9','0','0','0','0','100','0','500','500','0','0','53','1','28308','0','0','0','0','1','0','0','0','0','0','0','0','Captive Crocolisk - On Script - Start Waypoint');

-- Waypoints
SET @ENTRY_MOUNT := 28308;
DELETE FROM `waypoints` WHERE `entry`=@ENTRY_MOUNT;
INSERT INTO `waypoints` (`entry`, `pointid`, `position_x`, `position_y`, `position_z`, `point_comment`)
VALUES
(@ENTRY_MOUNT, 1, 5268.226, 4425.439, -95.55899, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 2, 5249.557, 4405.892, -96.04365, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 3, 5266.678, 4365.464, -98.19455, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 4, 5289.138, 4366.553, -102.234, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 5, 5296.972, 4363.764, -106.520, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 6, 5306.824, 4361.152, -111.009, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 7, 5315.826, 4355.953, -114.135, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 8, 5326.729, 4350.914, -117.849, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 9, 5337.281, 4344.719, -124.213, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 10, 5345.249, 4343.984, -129.590, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 11, 5355.099, 4341.122, -137.474, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 12, 5367.305, 4331.203, -142.851, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 13, 5374.383, 4322.940, -145.953, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 14, 5388.092, 4318.800, -146.897, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 15, 5400.322, 4315.879, -146.588, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 16, 5405.443, 4307.841, -142.03, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 17, 5434.999, 4305.659, -136.4706, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 18, 5464.708, 4302.066, -133.1981, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 19, 5490.555, 4294.395, -127.5203, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 20, 5503.808, 4269.717, -110.3168, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 21, 5518.324, 4255.308, -103.0638, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 22, 5540.53, 4259.77, -102.3979, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 23, 5564.194, 4263.45, -102.7574, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 24, 5585.45, 4261.137, -99.54807, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 25, 5609.614, 4259.657, -98.87333, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 26, 5633.434, 4259.228, -98.53442, 'Captive Crocolisk'), 
(@ENTRY_MOUNT, 27, 5681.639, 4266.31, -99.26748, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 28, 5708.126, 4273.348, -102.9183, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 29, 5748.732, 4284.135, -112.0557, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 30, 5839.82, 4368.61, -112.0805, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 31, 5865.922, 4371.208, -105.5544, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 32, 5870.382, 4377.948, -102.914, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 33, 5876.354, 4387.149, -98.657, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 34, 5897.276, 4408.44, -95.25065, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 35, 5925.311, 4440.624, -94.77592, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 36, 5953.005, 4476.29, -94.3763, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 37, 5964.229, 4503.729, -92.81553, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 38, 5960.583, 4546.558, -95.65462, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 39, 5965.167, 4579.141, -97.39779, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 40, 5969.295, 4613.739, -98.05751, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 41, 5970.536, 4617.309, -98.214, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 42, 5975.809, 4659.289, -99.27143, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 43, 5995.953, 4716.102, -97.779, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 44, 6036.854, 4788.551, -94.461, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 45, 6053.561, 4818.184, -94.571, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 46, 6066.358, 4835.206, -94.527, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 47, 6065.587, 4843.570, -92.827, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 48, 6064.670, 4858.181, -94.571, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 49, 6070.736, 4869.459, -94.464, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 50, 6085.825, 4884.223, -93.659, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 51, 6121.703, 4910.774, -95.314, 'Captive Crocolisk'),
(@ENTRY_MOUNT, 52, 6134.812, 4910.774, -94.922, 'Captive Crocolisk');