-- update quest_template 12983
UPDATE `quest_template` SET `RequiredNpcOrGo1`='29563', `RequiredNpcOrGoCount1`='1', `RequiredSourceItemId1`='42838', `RequiredSourceItemCount1`='1' WHERE `id`='12983';

-- spawn creature 29563
DELETE FROM `creature` WHERE `id`='29563';
INSERT INTO `creature` (`guid`,`id`,`map`,`spawnMask`,`phaseMask`,`modelid`,`equipment_id`,`position_x`,`position_y`,`position_z`,`orientation`,`spawntimesecs`,`spawndist`,`currentwaypoint`,`curhealth`,`curmana`,`MovementType`,`npcflag`,`unit_flags`,`dynamicflags`) VALUES
('152124','29563','571','1','2','0','0','7335.381','-2055.097','764.3585','3.368485','120','0','0','1','0','0','0','0','0');

UPDATE `creature_template` SET `npcflag`='16777216' WHERE `entry`='29563';

-- update phasemask creature 29605 29562
UPDATE `creature` SET `phaseMask`='3' WHERE `areaId`='4455' AND `id` IN ('29605', '29562');

-- smart_scripts NPC 30468
UPDATE `creature_template` SET `faction`='35', `AIName`='SmartAI' WHERE `entry`='30468';
DELETE FROM `smart_scripts` WHERE `entryorguid`='30468';
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
('30468','0','0','0','27','0','100','0','0','0','0','0','53','1','30468','0','12983','0','0','1','0','0','0','0','0','0','0','Harnessed Icemaw Matriarch - On Passenger - Start WP movement'),
('30468','0','1','0','40','0','100','0','1','30468','0','0','18','130','0','0','0','0','0','1','0','0','0','0','0','0','0','Harnessed Icemaw Matriarch - Reach Waypoint - Make Unatackable'),
('30468','0','2','0','40','0','100','0','50','30468','0','0','33','29563','0','0','0','0','0','23','0','0','0','0','0','0','0','Harnessed Icemaw Matriarch - Reach Waypoint - Quest Credit'),
('30468','0','3','0','40','0','100','0','50','30468','0','0','41','0','0','0','0','0','0','1','0','0','0','0','0','0','0','Harnessed Icemaw Matriarch - Reach Waypoint - Despawn'),
('30468','0','4','0','28','0','100','0','0','0','0','0','41','1','0','0','0','0','0','1','0','0','0','0','0','0','0','Harnessed Icemaw Matriarch - On Passenger Removed - Despawn Instant');

-- waypoints NPC 30468
DELETE FROM `waypoints` WHERE `entry`='30468';
INSERT INTO `waypoints` (`entry`, `pointid`, `position_x`, `position_y`, `position_z`, `point_comment`) VALUES
('30468','1','7339.62','-2058.66','764.919','Harnessed Icemaw Matriarch'),
('30468','2','7338.27','-2064.38','765.358','Harnessed Icemaw Matriarch'),
('30468','3','7335.77','-2073.8','767.408','Harnessed Icemaw Matriarch'),
('30468','4','7327.59','-2087.35','770.898','Harnessed Icemaw Matriarch'),
('30468','5','7319.45','-2095.41','773.681','Harnessed Icemaw Matriarch'),
('30468','6','7305.17','-2107.3','774.326','Harnessed Icemaw Matriarch'),
('30468','7','7275.18','-2114.63','775.668','Harnessed Icemaw Matriarch'),
('30468','8','7259','-2116.15','778.513','Harnessed Icemaw Matriarch'),
('30468','9','7241.65','-2119.36','777.765','Harnessed Icemaw Matriarch'),
('30468','10','7226.73','-2115.94','777.334','Harnessed Icemaw Matriarch'),
('30468','11','7208.44','-2115.22','770.951','Harnessed Icemaw Matriarch'),
('30468','12','7198.35','-2115.35','767.331','Harnessed Icemaw Matriarch'),
('30468','13','7193.22','-2115.25','765.634','Harnessed Icemaw Matriarch'),
('30468','14','7188.85','-2117.31','763.877','Harnessed Icemaw Matriarch'),
('30468','15','7177.06','-2123.51','762.934','Harnessed Icemaw Matriarch'),
('30468','16','7163.71','-2131.04','762.117','Harnessed Icemaw Matriarch'),
('30468','17','7146.6','-2130.74','762.099','Harnessed Icemaw Matriarch'),
('30468','18','7127.77','-2130.8','760.306','Harnessed Icemaw Matriarch'),
('30468','19','7130.19','-2108.96','761.682','Harnessed Icemaw Matriarch'),
('30468','20','7122.74','-2087.62','763.727','Harnessed Icemaw Matriarch'),
('30468','21','7114.4','-2070.32','765.977','Harnessed Icemaw Matriarch'),
('30468','22','7101.81','-2051.61','765.825','Harnessed Icemaw Matriarch'),
('30468','23','7091.48','-2031.1','765.895','Harnessed Icemaw Matriarch'),
('30468','24','7087.4','-2012.37','767.27','Harnessed Icemaw Matriarch'),
('30468','25','7081.41','-1985.07','767.962','Harnessed Icemaw Matriarch'),
('30468','26','7073.83','-1961.03','769.36','Harnessed Icemaw Matriarch'),
('30468','27','7068.84','-1934.14','775.735','Harnessed Icemaw Matriarch'),
('30468','28','7064.37','-1916.7','781.698','Harnessed Icemaw Matriarch'),
('30468','29','7070.38','-1906.56','785.498','Harnessed Icemaw Matriarch'),
('30468','30','7079.5','-1899.03','787.034','Harnessed Icemaw Matriarch'),
('30468','31','7085.34','-1887.63','788.909','Harnessed Icemaw Matriarch'),
('30468','32','7067.58','-1884.71','793.034','Harnessed Icemaw Matriarch'),
('30468','33','7041.7','-1884.61','797.428','Harnessed Icemaw Matriarch'),
('30468','34','7029.2','-1871.6','803.419','Harnessed Icemaw Matriarch'),
('30468','35','7025.07','-1858.88','811.24','Harnessed Icemaw Matriarch'),
('30468','36','7018.79','-1838.97','820.24','Harnessed Icemaw Matriarch'),
('30468','37','7011.7','-1814.38','820.73','Harnessed Icemaw Matriarch'),
('30468','38','7009.1','-1791.5','820.73','Harnessed Icemaw Matriarch'),
('30468','39','7017.04','-1758.97','819.654','Harnessed Icemaw Matriarch'),
('30468','40','7013.26','-1723.92','819.86','Harnessed Icemaw Matriarch'),
('30468','41','6995.1','-1720.75','820.112','Harnessed Icemaw Matriarch'),
('30468','42','6975.48','-1722.11','820.737','Harnessed Icemaw Matriarch'),
('30468','43','6959.88','-1724.39','820.596','Harnessed Icemaw Matriarch'),
('30468','44','6941.02','-1720.43','820.596','Harnessed Icemaw Matriarch'),
('30468','45','6920.03','-1709.56','820.753','Harnessed Icemaw Matriarch'),
('30468','46','6903','-1697.53','820.668','Harnessed Icemaw Matriarch'),
('30468','47','6886.75','-1682.95','820.258','Harnessed Icemaw Matriarch'),
('30468','48','6867.68','-1684.36','819.883','Harnessed Icemaw Matriarch'),
('30468','49','6847.06','-1695.64','819.986','Harnessed Icemaw Matriarch'),
('30468','50','6824.82','-1701.83','820.64','Harnessed Icemaw Matriarch');

-- creature_addon NPC 30468
DELETE FROM `creature_template_addon` WHERE `entry`='30468';
INSERT INTO `creature_template_addon` (`entry`,`mount`,`bytes1`,`bytes2`,`emote`,`auras`) VALUES
('30468','0','0','1','0', NULL);

-- npc_spellclick_spells NPC 30468
DELETE FROM `npc_spellclick_spells`  WHERE `npc_entry`='30468';
INSERT INTO `npc_spellclick_spells` (`npc_entry`, `spell_id`, `cast_flags`, `user_type`) VALUES
('30468','56795','1','0');

-- conditions
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`='18' AND `SourceGroup`='29563' AND `SourceEntry`='56795';
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES 
('18','29563','56795','0','0','9','0','12983','0','0','0','0','0', '', 'Required quest \'The Last of Her Kind\' active for spellclick');

-- spell_area
DELETE FROM `spell_area` WHERE `spell`='55857' AND `area`='4455';
INSERT INTO `spell_area`(`spell`,`area`,`quest_start`,`quest_end`,`aura_spell`,`racemask`,`gender`,`autocast`, `quest_start_status`, `quest_end_status`) VALUES 
('55857','4455','12983','12983','0','0','2','1','74','11');

