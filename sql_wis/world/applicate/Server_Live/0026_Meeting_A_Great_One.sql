-- Smart_scripts NPC 33533
DELETE FROM `smart_scripts` WHERE `entryorguid` = '33533' AND `id` = '1';
DELETE FROM `smart_scripts` WHERE `entryorguid` = '3353300';
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
('33533','0','1','0','38','0','100','1','1','1','0','0','80','3353300','2','0','0','0','0','1','0','0','0','0','0','0','0','On data 1 - Action list'),
('3353300','9','0','0','0','0','100','0','0','0','0','0','15','13956','0','0','0','0','0','23','0','0','0','0','0','0','0','Action List - Complete Quest 13956 Meeting a Great One'),
('3353300','9','1','0','0','0','100','0','3000','3000','0','0','1','7','0','0','0','0','0','1','0','0','0','0','0','0','0','Action List - Talk 7'),
('3353300','9','2','0','0','0','100','0','6000','6000','0','0','1','8','0','0','0','0','0','1','0','0','0','0','0','0','0','Action List - Talk 8'),
('3353300','9','3','0','0','0','100','0','5000','5000','0','0','1','9','0','0','0','0','0','1','0','0','0','0','0','0','0','Action List - Talk 9'),
('3353300','9','4','0','0','0','100','0','7000','7000','0','0','1','10','0','0','0','0','0','1','0','0','0','0','0','0','0','Action List - Talk 10');

-- Smart_scripts NPC 28092
DELETE FROM `smart_scripts` WHERE `entryorguid` = '28092' AND `id` = '1';
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
('28092','0','1','0','10','0','100','0','1','10','1','2','45','1','1','0','0','0','0','9','33533','1','10','0','0','0','0','LOS - Set data 1');