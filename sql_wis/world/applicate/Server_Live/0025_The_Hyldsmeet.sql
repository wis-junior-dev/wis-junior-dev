-- update quest_template 12970
UPDATE `quest_template` SET `RequiredNpcOrGo1`='30467', `RequiredNpcOrGoCount1`='1' WHERE `id`='12970';

-- smart_scripts NPC 29975
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`='29975';
DELETE FROM `smart_scripts` WHERE `entryorguid`='29975';
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
('29975','0','0','1','62','0','100','0','9910','0','0','0','33','30467','0','0','0','0','0','7','0','0','0','0','0','0','0','Lok\'lira the Crone - On Gossip Option 0 Selected - Quest Credit \'The Hyldsmeet\''),
('29975','0','1','0','61','0','100','0','0','0','0','0','72','0','0','0','0','0','0','7','0','0','0','0','0','0','0','Lok\'lira the Crone - On Gossip Option 0 Selected - Close Gossip');

-- conditions
DELETE FROM `conditions` WHERE `SourceGroup`='9907' AND `SourceTypeOrReferenceId`='15';
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
('15','9907','0','0','0','9','0','12970','0','0','0','0','0','','Loklira - Only show gossip 9907 when quest 12970 is added');