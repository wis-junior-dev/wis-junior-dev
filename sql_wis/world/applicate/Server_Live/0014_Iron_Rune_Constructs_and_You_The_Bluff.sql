DELETE FROM `spell_target_position` WHERE `id` = '49990';
INSERT INTO `spell_target_position` (`id`, `effIndex`, `target_map`, `target_position_x`, `target_position_y`, `target_position_z`, `target_orientation`) VALUES
('49990','0','571','478.952','-5941.53','308.75','0.419872');

-- Creature_template NPC 24823
DELETE FROM `creature_template` WHERE `entry`= '24823';
INSERT INTO `creature_template` (`entry`, `difficulty_entry_1`, `difficulty_entry_2`, `difficulty_entry_3`, `KillCredit1`, `KillCredit2`, `modelid1`, `modelid2`, `modelid3`, `modelid4`, `name`, `subname`, `IconName`, `gossip_menu_id`, `minlevel`, `maxlevel`, `exp`, `faction`, `npcflag`, `speed_walk`, `speed_run`, `scale`, `rank`, `mindmg`, `maxdmg`, `dmgschool`, `attackpower`, `dmg_multiplier`, `baseattacktime`, `rangeattacktime`, `unit_class`, `unit_flags`, `unit_flags2`, `dynamicflags`, `family`, `trainer_type`, `trainer_spell`, `trainer_class`, `trainer_race`, `minrangedmg`, `maxrangedmg`, `rangedattackpower`, `type`, `type_flags`, `lootid`, `pickpocketloot`, `skinloot`, `resistance1`, `resistance2`, `resistance3`, `resistance4`, `resistance5`, `resistance6`, `spell1`, `spell2`, `spell3`, `spell4`, `spell5`, `spell6`, `spell7`, `spell8`, `PetSpellDataId`, `VehicleId`, `mingold`, `maxgold`, `AIName`, `MovementType`, `InhabitType`, `HoverHeight`, `Health_mod`, `Mana_mod`, `Armor_mod`, `RacialLeader`, `questItem1`, `questItem2`, `questItem3`, `questItem4`, `questItem5`, `questItem6`, `movementId`, `RegenHealth`, `mechanic_immune_mask`, `flags_extra`, `ScriptName`, `VerifiedBuild`) VALUES
('24823','0','0','0','0','0','548','0','0','0','Iron Rune Construct','','','0','80','80','2','1194','0','1','1.14286','1','0','2','2','0','24','1','2000','2000','1','0','2048','0','0','0','0','0','0','1','1','0','9','0','0','0','0','0','0','0','0','0','0','44562','0','0','0','0','0','0','0','0','22','0','0','SmartAI','0','3','1','1','1','1','0','0','0','0','0','0','0','0','1','0','0','','12340');

-- Smart_scripts NPC 24823
DELETE FROM `smart_scripts` WHERE `entryorguid`='2482300' AND `source_type`='9';
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
('2482300','9','0','0','0','0','100','0','0','0','0','0','11','44569','0','0','0','0','0','23','0','0','0','0','0','0','0','Stanwad - Action list - Cast Credit');

-- Smart_scripts NPC 24718
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`='24718';

DELETE FROM `smart_scripts` WHERE `entryorguid`='24718' AND `source_type`='0';
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
('24718','0','0','0','10','0','100','0','1','5','60000','60000','80','2471800','0','0','0','0','0','1','0','0','0','0','0','0','0','Turd - LOS - Action list'),
('24718','0','1','0','8','1','100','0','44562','0','0','0','80','2471801','0','0','0','0','0','1','0','0','0','0','0','0','0','Lebronski - spell hit - action list');

DELETE FROM `smart_scripts` WHERE `entryorguid`='2471800' AND `source_type`='9';
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
('2471800','9','0','0','0','0','100','0','0','0','0','0','1','1','0','0','0','0','0','1','0','0','0','0','0','0','0','Stanwad - Action list - TALK'),
('2471800','9','1','0','0','0','100','0','0','0','0','0','22','1','0','0','0','0','0','1','0','0','0','0','0','0','0','Stanwad - Action list - Set Event phase1'),
('2471800','9','2','0','0','0','100','0','0','0','0','0','103','1','0','0','0','0','0','1','0','0','0','0','0','0','0','Stanwad - Action list - Root'),
('2471800','9','3','0','0','0','100','0','0','0','0','0','66','0','0','0','0','0','0','19','24823','20','0','0','0','0','0','Stanwad - Action list - Set Orientation'),
('2471800','9','4','0','0','0','100','0','30000','30000','0','0','103','0','0','0','0','0','0','1','0','0','0','0','0','0','0','Stanwad - Action list - remove root');

DELETE FROM `smart_scripts` WHERE `entryorguid`='2471801' AND `source_type`='9';
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
('2471801','9','0','0','0','0','100','0','0','0','0','0','22','0','0','0','0','0','0','1','0','0','0','0','0','0','0','Stanwad - Action list - Set Event phase 0'),
('2471801','9','1','0','0','0','100','0','0','0','0','0','1','0','0','0','0','0','0','19','24823','20','0','0','0','0','0','Stanwad - Action list - TALK'),
('2471801','9','2','0','0','0','100','0','5000','5000','0','0','1','2','0','0','0','0','0','1','0','0','0','0','0','0','0','Stanwad - Action list - TALK'),
('2471801','9','3','0','0','0','100','0','0','0','0','0','80','2482300','0','0','0','0','0','19','24823','20','0','0','0','0','0','Stanwad - Action list - Action list'),
('2471801','9','4','0','0','0','100','0','10000','10000','0','0','103','0','0','0','0','0','0','1','0','0','0','0','0','0','0','Stanwad - Action list - Pause wp');

-- Conditions
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`='22' AND `SourceGroup`='1' AND `SourceEntry`='24718';
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
('22','1','24718','0','0','31','0','3','24823','0','0','0','0','','event require npc 24823');

DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`='13' AND `SourceEntry`='44562';
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
('13','1','44562','0','0','31','0','3','24718','0','0','0','0','','Bluff target');

-- Creature_text NPC 24823
DELETE FROM `creature_text` WHERE `entry` ='24823';
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `TextRange`, `comment`) VALUES
('24823','0','0','Model U-9207 Iron Rune Construct does not appreciate your tone of voice. Commencing total annihilation of your rug, Lebronski.','12','0','100','0','0','0','0','Iron Rune Construct');

-- Creature_text NPC 24718
DELETE FROM `creature_text` WHERE `entry`='24718' AND `groupid` IN ('1', '2');
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `TextRange`, `comment`) VALUES
('24718','1','0','What do you think you\'re doing,  man? Lebronski does NOT appreciate you dragging your loose metal parts all over his rug.','12','7','100','0','0','0','0','Lebronski'),
('24718','2','0','Far out,  man. This bucket of bolts might make it after all...','12','7','100','0','0','0','0','Lebronski');