-- smart_scripts NPC 24538
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`= '24538';
DELETE FROM `smart_scripts` WHERE `entryorguid` = '24538';
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
('24538','0','0','1','8','0','100','0','43990','0','0','0','33','24538','0','0','0','0','0','7','0','0','0','0','0','0','0','On Spell Hit - kill credit'),
('24538','0','1','2','61','0','100','0','0','0','0','0','45','1','1','0','0','0','0','7','0','0','0','0','0','0','0','On Spell Hit - Set Data'),
('24538','0','2','0','61','0','100','0','0','0','0','0','11','57931','0','0','0','0','0','1','0','0','0','0','0','0','0','On Spell Hit - cast spell (fire)');

-- smart_scripts NPC 24646
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`= '24646';
DELETE FROM `smart_scripts` WHERE `entryorguid` = '24646';
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
('24646','0','0','1','8','0','100','0','43990','0','0','0','33','24646','0','0','0','0','0','7','0','0','0','0','0','0','0','On Spell Hit - kill credit'),
('24646','0','1','2','61','0','100','0','0','0','0','0','45','1','2','0','0','0','0','7','0','0','0','0','0','0','0','On Spell Hit - Set Data'),
('24646','0','2','0','61','0','100','0','0','0','0','0','11','57931','0','0','0','0','0','1','0','0','0','0','0','0','0','On Spell Hit - cast spell (fire)');

-- smart_scripts NPC 24647
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`= '24647';
DELETE FROM `smart_scripts` WHERE `entryorguid` = '24647';
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
('24647','0','0','1','8','0','100','0','43990','0','0','0','33','24647','0','0','0','0','0','7','0','0','0','0','0','0','0','On Spell Hit - kill credit'),
('24647','0','1','2','61','0','100','0','0','0','0','0','45','1','3','0','0','0','0','7','0','0','0','0','0','0','0','On Spell Hit - Set Data'),
('24647','0','2','0','61','0','100','0','0','0','0','0','11','57931','0','0','0','0','0','1','0','0','0','0','0','0','0','On Spell Hit - cast spell (fire)');

-- smart_scripts NPC 24533
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`= '24533';
DELETE FROM `smart_scripts` WHERE `entryorguid` = '24533';
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
('24533','0','0','0','0','0','100','0','500','500','1500','1500','11','44188','0','0','0','0','0','2','0','0','0','0','0','0','0','IC Update - Cast Spell Harpoon Toss'),
('24533','0','1','0','8','0','100','0','43997','0','0','0','37','0','0','0','0','0','0','1','0','0','0','0','0','0','0','On Spell Hit - Die');

-- smart_scripts NPC 27992
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`= '27992';
DELETE FROM `smart_scripts` WHERE `entryorguid` IN ('27992', '2799200');
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
('27992','0','0','1','25','0','100','0','0','0','0','0','20','0','0','0','0','0','0','1','0','0','0','0','0','0','0','On Reset - Set Auto Attack'),
('27992','0','1','0','61','0','100','0','0','0','0','0','21','0','0','0','0','0','0','1','0','0','0','0','0','0','0','On Reset - Set Combat Movement'),
('27992','0','2','5','38','0','100','0','1','1','0','0','1','0','0','0','0','0','0','1','0','0','0','0','0','0','0','On Data Set - Talk'),
('27992','0','3','5','38','0','100','0','1','2','0','0','1','1','0','0','0','0','0','1','0','0','0','0','0','0','0','On Data Set - Talk'),
('27992','0','4','5','38','0','100','0','1','3','0','0','1','2','0','0','0','0','0','1','0','0','0','0','0','0','0','On Data Set - Talk'),
('27992','0','5','0','61','0','100','0','0','0','0','0','80','2799200','2','0','0','0','0','1','0','0','0','0','0','0','0','On Data Set - Run Script'),
('27992','0','6','0','31','0','100','0','43997','0','0','0','11','43998','0','0','0','0','0','1','0','0','0','0','0','0','0','On Spell Hit Target - Cast Spell'),
('2799200','9','0','0','0','0','100','0','3000','3000','0','0','12','24533','4','60000','0','1','0','8','0','0','0','970','-5250','195','4.5','Script - Summon Dragonflayer Defender'),
('2799200','9','1','0','0','0','100','0','3000','3000','0','0','12','24533','4','60000','0','1','0','8','0','0','0','978','-5272','204','4.5','Script - Summon Dragonflayer Defender'),
('2799200','9','2','0','0','0','100','0','3000','3000','0','0','12','24533','4','60000','0','1','0','8','0','0','0','956','-5267','198','4.5','Script - Summon Dragonflayer Defender');