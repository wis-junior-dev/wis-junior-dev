-- smart_scripts NPC 14688
UPDATE `creature_template` SET `AIName`= 'SmartAI' WHERE `entry`='14688';
DELETE FROM `smart_scripts` WHERE `entryorguid` IN ('14688', '1468800');
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
('14688','0','0','0','0','0','80','0','0','0','10000','20000','11','61162','0','0','0','0','0','2','0','0','0','0','0','0','0','Prince Sandoval - IC - Cast Engulfing Strike'),
('14688','0','1','0','0','0','80','0','15000','20000','30000','30000','11','61163','0','0','0','0','0','2','0','0','0','0','0','0','0','Prince Sandoval - IC - Cast Fire Nova'),
('14688','0','2','0','0','0','100','0','30000','30000','30000','30000','11','61144','1','0','0','0','0','1','0','0','0','0','0','0','0','Prince Sandoval - IC - Cast Fire Shield'),
('14688','0','3','4','0','0','100','0','31100','31100','30000','30000','11','61145','1','0','0','0','0','1','0','0','0','0','0','0','0','Prince Sandoval - IC - Cast Ember Shower'),
('14688','0','4','5','61','0','100','0','0','0','0','0','21','0','0','0','0','0','0','1','0','0','0','0','0','0','0','Prince Sandoval - On Link - Change state'),
('14688','0','5','6','61','0','100','0','0','0','0','0','18','131077','0','0','0','0','0','1','0','0','0','0','0','0','0','Prince Sandoval - On Link - Change unit flag'),
('14688','0','6','0','61','0','100','0','0','0','0','0','103','1','0','0','0','0','0','1','0','0','0','0','0','0','0','Prince Sandoval - On Link - Set Root'),
('14688','0','7','8','0','0','100','0','45100','45100','30000','30000','19','131077','0','0','0','0','0','1','0','0','0','0','0','0','0','Prince Sandoval - IC - Change unit flag'),
('14688','0','8','9','61','0','100','0','0','0','0','0','21','1','0','0','0','0','0','1','0','0','0','0','0','0','0','Prince Sandoval - On Link - Change state'),
('14688','0','9','10','61','0','100','0','0','0','0','0','28','61144','0','0','0','0','0','1','0','0','0','0','0','0','0','Prince Sandoval - On Link - Remove Fire Shield'),
('14688','0','10','11','61','0','100','0','0','0','0','0','28','61145','0','0','0','0','0','1','0','0','0','0','0','0','0','Prince Sandoval - On Link- Remove Ember Shower'),
('14688','0','11','0','61','0','100','0','0','0','0','0','103','0','0','0','0','0','0','1','0','0','0','0','0','0','0','Prince Sandoval - On Link - Remove Root'),
('14688','0','12','0','54','0','100','0','0','0','0','0','80','1468800','0','0','0','0','0','1','0','0','0','0','0','0','0','Prince Sandoval - On  summon - Call script 1'),
('14688','0','13','0','54','0','100','0','0','0','0','0','11','4335','2','0','0','0','0','1','0','0','0','0','0','0','0','Prince Sandoval - On  summon - Cast Summon Smoke'),
('14688','0','14','0','54','0','100','1','0','0','0','0','1','0','0','0','0','0','0','1','0','0','0','0','0','0','0','Prince Sandoval - OOC script 1 - Say 3'),
('14688','0','15','0','1','0','100','0','14000','14000','0','0','49','0','0','0','0','0','0','21','100','0','0','0','0','0','0','Prince Sandoval - OOC - Start Attack'),
('14688','0','16','0','5','0','100','0','0','0','0','0','45','20','20','0','0','0','0','11','31135','100','0','0','0','0','0','Prince Sandoval - On kill target - Set Data'),
('14688','0','17','0','6','0','100','0','0','0','1','0','45','6','6','0','0','0','0','11','31135','100','0','0','0','0','0','Prince Sandoval - On death - Set Data'),
('14688','0','18','0','1','0','100','1','30000','30000','0','0','45','19','19','0','0','0','0','11','31135','100','0','0','0','0','0','Prince Sandoval - OOC - Set Data'),
('14688','0','19','20','1','0','100','0','0','0','0','0','19','131077','0','0','0','0','0','1','0','0','0','0','0','0','0','Prince Sandoval - OOC - Change state'),
('14688','0','20','21','61','0','100','0','0','0','0','0','28','61144','0','0','0','0','0','1','0','0','0','0','0','0','0','Prince Sandoval - On Link - Remove Fire Shield'),
('14688','0','21','22','61','0','100','0','0','0','0','0','28','61145','0','0','0','0','0','1','0','0','0','0','0','0','0','Prince Sandoval - On Link- Remove Ember Shower'),
('14688','0','22','0','61','0','100','0','0','0','0','0','103','0','0','0','0','0','0','1','0','0','0','0','0','0','0','Prince Sandoval - On Link - Remove Root'),
('1468800','9','0','0','1','0','100','1','4000','4000','0','0','1','1','10','0','0','0','0','1','0','0','0','0','0','0','0','Prince Sandoval - OOC script 1 - Say 1'),
('1468800','9','1','0','1','0','100','1','0','0','0','0','69','0','0','0','0','0','0','8','0','0','0','8197.6','3502.56','625.108','0.585','Prince Sandoval - OOC script 1 - Move to position'),
('1468800','9','2','0','1','0','100','1','8000','8000','0','0','1','2','10','0','0','0','0','1','0','0','0','0','0','0','0','Prince Sandoval - OOC script 1 - Say 2');

-- creature_text NPC 14688
DELETE FROM `creature_text` WHERE `entry` = '14688';
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `TextRange`, `comment`) VALUES
('14688','0','0','Hardly a fitting introduction , Spear-Wife. Now, who is this outsider that I''ve been hearing so much about?','14','0','0','0','0','0','0','Prince Sandoval say 1'),
('14688','1','0','I will make this as easy as possible for you. Simply come here and die. That is all that I ask for your many trespasses. For your sullying this exlated place of battle.','14','0','0','0','0','0','0','Prince Sandoval say 2'),
('14688','2','0','FOR YOUR EFFRONTERY TO THE LICH KING!','14','0','0','0','0','0','0','Prince Sandoval say 3');