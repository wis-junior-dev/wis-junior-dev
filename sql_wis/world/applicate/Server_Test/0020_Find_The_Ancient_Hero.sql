-- smart_scripts NPC 30718
UPDATE `creature_template` SET AIName='SmartAI', unit_flags='0' WHERE `entry`= '30718';
DELETE FROM `smart_scripts` WHERE `entryorguid` IN ('30718',' 3071800', '3071801');
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
('30718','0','0','0','62','0','100','0','10008','0','0','0','88','3071800','3071801','0','0','0','0','1','0','0','0','0','0','0','0','Slumbering Mjordin - Gossip - Random Script'),
('3071800','9','0','0','0','0','100','0','500','500','0','0','2','14','0','0','0','0','0','1','0','0','0','0','0','0','0','Slumbering Mjordin - Script - Enemy'),
('3071800','9','1','0','0','0','100','0','1000','1000','0','0','8','2','0','0','0','0','0','1','0','0','0','0','0','0','0','Slumbering Mjordin - Script - Aggresive'),
('3071801','9','0','0','0','0','100','0','1000','1000','0','0','12','30884','3','180000','0','0','0','1','0','0','0','0','0','0','0','Slumbering Mjordin - Script - Summon Iskalder'),
('3071801','9','1','0','0','0','100','0','0','0','0','0','47','0','0','0','0','0','0','1','0','0','0','0','0','0','0','Slumbering Mjordin - Script - Set visibility off'),
('3071801','9','2','0','0','0','100','0','0','0','0','0','41','0','0','0','0','0','0','1','0','0','0','0','0','0','0','Slumbering Mjordin - Script - Despawn');

-- smart_scripts NPC 30886
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`= '30886';
DELETE FROM `smart_scripts` WHERE `entryorguid` = '30886';
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
('30886','0','0','0','0','0','100','1','1','1','1','1','18','512','0','0','0','0','0','1','0','0','0','0','0','0','0','Subjugated Iskalder - On IC - Disables combat with NPC'),
('30886','0','1','0','54','0','100','0','0','0','0','0','29','0','0','30232','0','1','0','7','0','0','0','0','0','0','0','Subjugated Iskalder - On spawn - Follow player'),
('30886','0','2','0','65','0','100','0','0','0','0','0','11','25729','0','0','0','0','0','23','0','0','0','0','0','0','0','Subjugated Iskalder - On follow complete - Quest credit');

-- smart_scripts NPC 30884
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`= '30884';
DELETE FROM `smart_scripts` WHERE `entryorguid` = '30884';
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
('30884','0','0','0','54','0','100','0','0','0','0','0','1','1','100','0','0','0','0','1','0','0','0','0','0','0','0','Iskalder - On spawn - Say text'),
('30884','0','1','0','8','0','100','0','3921','0','0','0','41','5100','0','0','0','0','0','1','0','0','0','0','0','0','0','Iskalder - On spell hit - Despawn');

-- conditions
DELETE FROM `conditions` WHERE `SourceEntry`= '3921';
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
('13','2','3921','0','0','31','1','3','30884','0','0','0','0','','The purple beam effect of amulet can target only Iskalder'),
('17','0','3921','0','0','31','1','3','30884','0','0','0','0','','Amulet can target only Iskalder');

-- creature_text NPC 
DELETE FROM `creature_text` WHERE `entry` = '30884';
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `TextRange`, `comment`) VALUES
('30884','1','1','You have found him! Now is the time to use the The Bone Witch\"s Amulet!','41','0','100','0','100','0','0','Iskalder - Say on spawn');