-- NPC Crusader Jonathan 28136
UPDATE `creature_template` SET `gossip_menu_id`='9660', `npcflag`='1', `unit_flags`='131074', `AIName`='SmartAI' WHERE `entry`='28136';

DELETE FROM `smart_scripts` WHERE `entryorguid` IN ('28136', '2813600');
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
('28136','0','0','1','54','0','100','0','0','0','0','0','11','50665','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Jonathan - Just summoned - Spellcast Bleeding Out'),
('28136','0','1','2','61','0','100','0','0','0','0','0','64','1','0','0','0','0','0','7','0','0','0','0','0','0','0','Crusader Jonathan - Just summoned - Store targetlist'),
('28136','0','2','3','61','0','100','0','0','0','0','0','29','0','0','0','0','0','0','7','0','0','0','0','0','0','0','Crusader Jonathan - Just summoned - Follow envoker'),
('28136','0','3','4','61','0','100','0','0','0','0','0','22','1','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Jonathan - Just summoned - Set phase 1'),
('28136','0','4','0','61','0','100','0','0','0','0','0','91','1','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Jonathan - Just summoned - Reset unit_field_bytes1'),
('28136','0','5','0','23','1','100','1','50665','0','0','0','80','2813600','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Jonathan - Aura Bleeding Out missing (phase 1) - Call timed actionlist'),
('28136','0','6','7','40','0','100','1','5','0','0','0','90','1','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Jonathan - On WP 5 reached - Set unit_field_bytes1'),
('28136','0','7','0','61','0','100','0','0','0','0','0','41','20000','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Jonathan - On WP 5 reached - Despawn after 20 seconds'),
('28136','0','8','9','8','1','100','0','50669','0','0','0','22','2','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Jonathan - On spellhit Quest Credit - Set phase 2'),
('28136','0','9','10','61','0','100','0','0','0','0','0','11','50671','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Jonathan - On spellhit Quest Credit - Cast Kill Credit Jonathan 01'),
('28136','0','10','11','61','0','100','0','0','0','0','0','11','50709','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Jonathan - On spellhit Quest Credit - Cast Strip Aura Jonathan 01'),
('28136','0','11','12','61','0','100','0','0','0','0','0','86','50680','0','12','1','0','0','1','0','0','0','0','0','0','0','Crusader Jonathan - On spellhit Quest Credit - Crosscast Kill Credit Jonathan'),
('28136','0','12','13','61','0','100','0','0','0','0','0','86','50710','0','12','1','0','0','1','0','0','0','0','0','0','0','Crusader Jonathan - On spellhit Quest Credit - Crosscast Strip Aura Jonanthan'),
('28136','0','13','14','61','0','100','0','0','0','0','0','29','0','0','0','0','0','0','0','0','0','0','0','0','0','0','Crusader Jonathan - On spellhit Quest Credit - Stop follow'),
('28136','0','14','15','61','0','100','0','0','0','0','0','1','0','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Jonathan - On spellhit Quest Credit - Say text'),
('28136','0','15','16','61','0','100','0','0','0','0','0','53','0','28136','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Jonathan - On spellhit Quest Credit - Start WP movement'),
('28136','0','16','0','61','0','100','0','0','0','0','0','83','1','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Jonathan - On spellhit Quest Credit - Remove npcflag'),
('2813600','9','0','0','0','0','100','0','0','0','0','0','1','1','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Jonathan script - Say text'),
('2813600','9','1','0','0','0','100','0','0','0','0','0','22','2','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Jonathan script - Set phase 2'),
('2813600','9','2','0','0','0','100','0','0','0','0','0','29','0','0','0','0','0','0','0','0','0','0','0','0','0','0','Crusader Jonathan script - Stop follow'),
('2813600','9','3','0','0','0','100','0','2000','2000','0','0','37','0','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Jonathan script - Die');

DELETE FROM `waypoints` WHERE `entry` ='28136';
INSERT INTO `waypoints` (`entry`,`pointid`,`position_x`,`position_y`,`position_z`) VALUES
('28136','1','5257.454','-3500.14','291.6933'),
('28136','2','5253.089','-3516.885','291.6786'),
('28136','3','5255.452','-3523.673','291.6932'),
('28136','4','5262.733','-3527.41','291.6934'),
('28136','5','5261.445','-3528.885','291.6929');

-- NPC Crusader Lamoof 28142
UPDATE `creature_template` SET `gossip_menu_id`='9659', `npcflag`='1', `unit_flags`='131074', `AIName`='SmartAI' WHERE `entry`='28142';

DELETE FROM `smart_scripts` WHERE `entryorguid` IN ('28142', '2814200');
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
('28142','0','0','1','54','0','100','0','0','0','0','0','11','50681','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Lamoof - Just summoned - Spellcast Bleeding Out'),
('28142','0','1','2','61','0','100','0','0','0','0','0','64','1','0','0','0','0','0','7','0','0','0','0','0','0','0','Crusader Lamoof - Just summoned - Store targetlist'),
('28142','0','2','3','61','0','100','0','0','0','0','0','29','0','0','0','0','0','0','7','0','0','0','0','0','0','0','Crusader Lamoof - Just summoned - Follow envoker'),
('28142','0','3','4','61','0','100','0','0','0','0','0','22','1','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Lamoof - Just summoned - Set phase 1'),
('28142','0','4','0','61','0','100','0','0','0','0','0','91','1','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Lamoof - Just summoned - Reset unit_field_bytes1'),
('28142','0','5','0','23','1','100','1','50681','0','0','0','80','2814200','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Lamoof - Aura Bleeding Out missing (phase 1) - Call timed actionlist'),
('28142','0','6','7','40','0','100','1','5','0','0','0','90','1','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Lamoof - On WP 5 reached - Set unit_field_bytes1'),
('28142','0','7','0','61','0','100','0','0','0','0','0','41','20000','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Lamoof - On WP 5 reached - Despawn after 20 seconds'),
('28142','0','8','9','8','1','100','0','50669','0','0','0','22','2','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Lamoof - On spellhit Quest Credit - Set phase 2'),
('28142','0','9','10','61','0','100','0','0','0','0','0','11','50683','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Lamoof - On spellhit Quest Credit - Cast Kill Credit Lamoof 01'),
('28142','0','10','11','61','0','100','0','0','0','0','0','11','50723','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Lamoof - On spellhit Quest Credit - Cast Strip Aura Lamoof 01'),
('28142','0','11','12','61','0','100','0','0','0','0','0','86','50684','0','12','1','0','0','1','0','0','0','0','0','0','0','Crusader Lamoof - On spellhit Quest Credit - Crosscast Kill Credit Lamoof'),
('28142','0','12','13','61','0','100','0','0','0','0','0','86','50722','0','12','1','0','0','1','0','0','0','0','0','0','0','Crusader Lamoof - On spellhit Quest Credit - Crosscast Strip Aura Lamoof'),
('28142','0','13','14','61','0','100','0','0','0','0','0','29','0','0','0','0','0','0','0','0','0','0','0','0','0','0','Crusader Lamoof - On spellhit Quest Credit - Stop follow'),
('28142','0','14','15','61','0','100','0','0','0','0','0','1','0','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Lamoof - On spellhit Quest Credit - Say text'),
('28142','0','15','16','61','0','100','0','0','0','0','0','53','0','28142','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Lamoof - On spellhit Quest Credit - Start WP movement'),
('28142','0','16','0','61','0','100','0','0','0','0','0','83','1','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Lamoof - On spellhit Quest Credit - Remove npcflag'),
('2814200','9','0','0','0','0','100','0','0','0','0','0','1','1','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Lamoof script - Say text'),
('2814200','9','1','0','0','0','100','0','0','0','0','0','22','2','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Lamoof script - Set phase 2'),
('2814200','9','2','0','0','0','100','0','0','0','0','0','29','0','0','0','0','0','0','0','0','0','0','0','0','0','0','Crusader Lamoof script - Stop follow'),
('2814200','9','3','0','0','0','100','0','2000','2000','0','0','37','0','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Lamoof script - Die');

DELETE FROM `waypoints` WHERE `entry` ='28142';
INSERT INTO `waypoints` (`entry`,`pointid`,`position_x`,`position_y`,`position_z`) VALUES
('28142','1','5257.454','-3500.14','291.6933'),
('28142','2','5253.089','-3516.885','291.6786'),
('28142','3','5255.452','-3523.673','291.6932'),
('28142','4','5260.741','-3525.38','291.69343'),
('28142','5','5259.029','-3527.101','291.4913');

-- NPC Crusader Josephine 28148
UPDATE `creature_template` SET `gossip_menu_id`='9658', `npcflag`='1', `unit_flags`='131074', `AIName`='SmartAI' WHERE `entry`='28148';

DELETE FROM `smart_scripts` WHERE `entryorguid` IN ('28148', '2814800');
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
('28148','0','0','1','54','0','100','0','0','0','0','0','11','50695','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Josephine - Just summoned - Spellcast Bleeding Out'),
('28148','0','1','2','61','0','100','0','0','0','0','0','64','1','0','0','0','0','0','7','0','0','0','0','0','0','0','Crusader Josephine - Just summoned - Store targetlist'),
('28148','0','2','3','61','0','100','0','0','0','0','0','29','0','0','0','0','0','0','7','0','0','0','0','0','0','0','Crusader Josephine - Just summoned - Follow envoker'),
('28148','0','3','4','61','0','100','0','0','0','0','0','22','1','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Josephine - Just summoned - Set phase 1'),
('28148','0','4','0','61','0','100','0','0','0','0','0','91','1','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Josephine - Just summoned - Reset unit_field_bytes1'),
('28148','0','5','0','23','1','100','1','50695','0','0','0','80','2814800','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Josephine - Aura Bleeding Out missing (phase 1) - Call timed actionlist'),
('28148','0','6','7','40','0','100','1','4','0','0','0','90','1','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Josephine - On WP 4 reached - Set unit_field_bytes1'),
('28148','0','7','0','61','0','100','0','0','0','0','0','41','20000','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Josephine - On WP 4 reached - Despawn after 20 seconds'),
('28148','0','8','9','8','1','100','0','50669','0','0','0','22','2','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Josephine - On spellhit Quest Credit - Set phase 2'),
('28148','0','9','10','61','0','100','0','0','0','0','0','11','50698','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Josephine - On spellhit Quest Credit - Cast Kill Credit Jospehine 01'),
('28148','0','10','11','61','0','100','0','0','0','0','0','11','50711','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Josephine - On spellhit Quest Credit - Cast Strip Aura Josephine 01'),
('28148','0','11','12','61','0','100','0','0','0','0','0','86','50699','0','12','1','0','0','1','0','0','0','0','0','0','0','Crusader Josephine - On spellhit Quest Credit - Crosscast Kill Credit Josephine'),
('28148','0','12','13','61','0','100','0','0','0','0','0','86','50712','0','12','1','0','0','1','0','0','0','0','0','0','0','Crusader Josephine - On spellhit Quest Credit - Crosscast Strip Aura Josephine'),
('28148','0','13','14','61','0','100','0','0','0','0','0','29','0','0','0','0','0','0','0','0','0','0','0','0','0','0','Crusader Josephine - On spellhit Quest Credit - Stop follow'),
('28148','0','14','15','61','0','100','0','0','0','0','0','1','0','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Josephine - On spellhit Quest Credit - Say text'),
('28148','0','15','16','61','0','100','0','0','0','0','0','53','0','28148','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Josephine - On spellhit Quest Credit - Start WP movement'),
('28148','0','16','0','61','0','100','0','0','0','0','0','83','1','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Josephine - On spellhit Quest Credit - Remove npcflag'),
('2814800','9','0','0','0','0','100','0','0','0','0','0','1','1','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Josephine script - Say text'),
('2814800','9','1','0','0','0','100','0','0','0','0','0','22','2','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Josephine script - Set phase 2'),
('2814800','9','2','0','0','0','100','0','0','0','0','0','29','0','0','0','0','0','0','0','0','0','0','0','0','0','0','Crusader Josephine script - Stop follow'),
('2814800','9','3','0','0','0','100','0','2000','2000','0','0','37','0','0','0','0','0','0','1','0','0','0','0','0','0','0','Crusader Josephine script - Die');

DELETE FROM `waypoints` WHERE `entry` ='28148';
INSERT INTO `waypoints` (`entry`,`pointid`,`position_x`,`position_y`,`position_z`) VALUES
('28148','1','5257.454','-3500.14','291.6933'),
('28148','2','5253.089','-3516.885','291.6786'),
('28148','3','5257.784','-3521.994','291.6931'),
('28148','4','5256.293','-3523.494','291.6933');

-- NPC Leave No One Behind Bunny
DELETE FROM `creature` WHERE `guid` = '120464' AND `id` = '28158';
DELETE FROM `creature` WHERE `id` = '28137';
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES
('117936','28137','571','0','0','1','1','0','0','5257.56','-3499.99','291.759','4.31096','300','0','0','4979','0','0','0','0','0');

UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry`='28137';
DELETE FROM `smart_scripts` WHERE `entryorguid` = '28137';
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
('28137','0','0','0','1','0','100','0','2000','2000','2000','2000','11','50669','0','0','0','0','0','1','0','0','0','0','0','0','0','Leave No One Behind Bunny - Out of Combat - Cast \'Quest Credit\'');


DELETE FROM `conditions` WHERE `SourceEntry` = '50669';
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
('13','1','50669','0','0','31','0','3','28142','0','0','0','0','','Spell Quest Credit targets Crusader Lamoof'),
('13','1','50669','0','1','31','0','3','28136','0','0','0','0','','Spell Quest Credit targets Crusader Jonothan'),
('13','1','50669','0','2','31','0','3','28148','0','0','0','0','','Spell Quest Credit targets Crusader Josephine');

DELETE FROM `creature_text` WHERE `entry` IN ('28136', '28142', '28148') AND `groupid`='1';
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES
('28136','1','0','<sigh>... This is the end of me.','12','0','100','1','0','0','Crusader Jonathan'),
('28142','1','0','<sigh>... This is the end of me.','12','0','100','1','0','0','Crusader Lamoof'),
('28148','1','0','Ohhh, I... cannot go on....','12','0','100','1','0','0','Crusader Josephine');