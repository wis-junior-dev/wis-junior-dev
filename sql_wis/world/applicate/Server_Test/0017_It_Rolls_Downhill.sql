-- smart_scripts NPC 28750
UPDATE `creature_template` SET `AIName`='SmartAI', `spell1`='52245' WHERE `entry`='28750';
DELETE FROM `smart_scripts` WHERE `entryorguid`IN ('28750','2875000');
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
('28750','0','0','1','8','0','100','0','52245','0','0','0','69','0','0','0','0','0','0','13','190716','5','30','0','0','0','0','On Spellhit - Move to pos - Blight1'),
('28750','0','1','0','61','0','100','0','0','0','0','0','80','2875000','2','0','0','0','0','1','0','0','0','0','0','0','0','On Spellhit - Move to pos - Blight1'),
('28750','0','2','3','8','0','100','0','52245','0','0','0','69','0','0','0','0','0','0','13','190939','5','30','0','0','0','0','On Spellhit - Move to pos - Blight3'),
('28750','0','3','0','61','0','100','0','0','0','0','0','80','2875000','2','0','0','0','0','1','0','0','0','0','0','0','0','On Spellhit Move to pos - Blight2'),
('28750','0','4','5','8','0','100','0','52245','0','0','0','69','0','0','0','0','0','0','13','190940','5','30','0','0','0','0','On Spellhit - Move to pos - Blight3'),
('28750','0','5','0','61','0','100','0','0','0','0','0','80','2875000','2','0','0','0','0','1','0','0','0','0','0','0','0','On Spellhit Move to pos - Blight3'),
('28750','0','6','0','8','0','100','0','52244','0','0','0','1','0','0','0','0','0','0','1','0','0','0','0','0','0','0','On Spellhit - Say Random'),
('28750','0','7','0','8','0','100','0','52252','0','0','0','11','52243','2','0','0','0','0','1','0','0','0','0','0','0','0','On Spellhit - Say Radiation'),
('2875000','9','0','0','0','0','100','0','4000','4000','4000','4000','11','52247','2','0','0','0','0','1','0','0','0','0','0','0','0',' - On Script - '),
('2875000','9','1','0','0','0','100','0','4000','4000','4000','4000','11','61456','2','0','0','0','0','1','0','0','0','0','0','0','0',' - On Script - '),
('2875000','9','2','0','0','0','100','1','2000','2000','2000','2000','11','52248','0','0','0','0','0','1','0','0','0','0','0','0','0',' - On Script - '),
('2875000','9','3','0','0','0','100','0','1000','1000','1000','1000','69','0','0','0','0','0','0','1','0','0','0','6174.28','-2017.25','245.116','0','On move inform 0 Cast credit'),
('2875000','9','4','0','0','0','100','0','0','0','0','0','41','10000','0','0','0','0','0','1','0','0','0','0','0','0','0','On link - Despawn - Self');

-- conditions
DELETE FROM `conditions` WHERE `SourceEntry`='28750';
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
('22','4','28750','1','0','30','1','190940','1','0','1','0','0','','Execute only if GObject3 is in range'),
('22','3','28750','1','0','30','1','190939','1','0','1','0','0','','Execute only if GObject2 is in range'),
('22','1','28750','1','0','30','1','190716','1','0','1','0','0','','Execute only if GObject1 is in range');

-- creature_text NPC 28750
DELETE FROM `creature_text` WHERE `entry`='28750';
INSERT INTO `creature_text` (`entry`,`groupid`,`id`,`text`,`type`,`language`,`probability`,`emote`,`duration`,`sound`,`comment`) VALUES
('28750','0','1','Mphmm rmphhimm rrhumghph?','12','0','100','1','0','0','Blight Geist'),
('28750','0','2','Mhrrumph rummrhum phurr!','12','0','100','1','0','0','Blight Geist');