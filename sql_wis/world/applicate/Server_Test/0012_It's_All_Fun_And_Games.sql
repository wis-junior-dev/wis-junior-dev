-- NPC 29790
UPDATE `creature_template` SET `InhabitType`='4', `unit_flags`='33555204' WHERE `entry`='29790';

-- NPC 29747
UPDATE `creature_template` SET `InhabitType`='4', `AIName`='SmartAI', unit_flags='4' WHERE `entry`='29747';

DELETE FROM `smart_scripts` WHERE `entryorguid` IN ('29747', '2974700');
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
('29747','0','0','1','25','0','100','1','0','0','0','0','75','55162','0','0','0','0','0','1','0','0','0','0','0','0','0','Ocular - On spawn/reset - Add aura transform'),
('29747','0','1','0','61','0','100','1','0','0','0','0','103','1','0','0','0','0','0','1','0','0','0','0','0','0','0','Ocular - Linked with previous event - Set root state on'),
('29747','0','2','0','25','0','100','0','0','0','0','0','8','1','0','0','0','0','0','1','0','0','0','0','0','0','0','Ocular - On spawn/reset - Set react state defensive'),
('29747','0','3','4','8','0','100','1','30740','0','0','0','8','2','0','0','0','0','0','1','0','0','0','0','0','0','0','Ocular - On hit by item spell - Set react state aggressive (once per reset)'),
('29747','0','4','0','61','0','100','0','0','0','0','0','11','55269','0','0','0','0','0','7','0','0','0','0','0','0','0','Ocular - Linked with previous event - Cast spell on invoker to set in combat'),
('29747','0','5','0','0','0','100','0','1500','1500','4000','5000','11','55269','0','0','0','0','0','2','0','0','0','0','0','0','0','Ocular - IC - Cast Deathly Stare every 3 secs'),
('29747','0','6','7','6','0','100','0','0','0','0','0','85','55288','0','0','0','0','0','1','0','0','0','0','0','0','0','Ocular - On death - Cast The Ocular On Death'),
('29747','0','7','0','61','0','100','0','0','0','0','0','80','2974700','2','0','0','0','0','1','0','0','0','0','0','0','0','Ocular - Linked with previous event - Start actionlist'),
('2974700','9','0','0','0','0','100','0','2000','2000','0','0','47','0','0','0','0','0','0','10','152166','29790','0','0','0','0','0','Ocular - Action 0 - Set unseen on shell'),
('2974700','9','1','0','0','0','100','0','0','0','0','0','47','0','0','0','0','0','0','1','0','0','0','0','0','0','0','Ocular - Action 1 - Set unseen on self'),
('2974700','9','2','0','0','0','100','0','0','0','0','0','41','100','0','0','0','0','0','10','152166','29790','0','0','0','0','0','Ocular - Action 2 - Despawn shell'),
('2974700','9','3','0','0','0','100','0','0','0','0','0','41','100','0','0','0','0','0','1','0','0','0','0','0','0','0','Ocular - Action 3 - Despawn self');

-- Creature_template_addon NPC 29747, 29790
DELETE FROM `creature_template_addon` WHERE `entry` IN ('29747', '29790');
INSERT INTO `creature_template_addon` (`entry`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES
('29747','0','0','0','1','0','18950'),
('29790','0','0','0','1','0','18950');

-- Spawn NPC 29747, 29790
DELETE FROM `creature` WHERE `id` IN ('29790', '29747');
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES
('152165','29747','571','0','0','1','2','0','0','8526.23','2665.08','1045.04','2.67035','120','0','0','126000','0','0','0','0','0'),
('152166','29790','571','0','0','1','2','0','0','8526.23','2665.09','1037.09','2.67035','120','0','0','1','0','0','0','0','0');

-- Quest_template 12892, 12887
UPDATE `quest_template` SET `Flags` = '128', `RequiredNpcOrGo1` = '29803', `RequiredNpcOrGoCount1` = '1' WHERE `id` IN ('12892', '12887');

-- Conditions
DELETE FROM `conditions` WHERE `SourceEntry`='55288' AND `SourceTypeOrReferenceId`='13';
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
('13','1','55288','0','0','32','0','144','0','0','0','0','0','','Death of Ocular can hit only players'),
('13','1','55288','0','1','9','0','12892','0','0','0','0','0','','It\'s all fun and games (12892 H) must be taken'),
('13','1','55288','0','1','28','0','12892','0','0','1','0','0','','It\'s all fun and games (12892 H) must not have objectives completed'),
('13','1','55288','0','2','9','0','12887','0','0','0','0','0','','It\'s all fun and games (12887 A) must be taken'),
('13','1','55288','0','2','28','0','12887','0','0','1','0','0','','It\'s all fun and games (12887 A) must not have objectives completed');