-- entry trash 10 man 34269, 34271, 34273, 34135, 34134, 34137, 33823, 33824, 33820, 33822,33819, 33818, 33699, 33722, 34196, 34267, 33527, 34069, 33355, 33525, 34086, 34199, 33526, 34198, 34190, 33528, 33430, 34085,33431, 33772, 33754, 33755, 33354, 34193, 34133, 34197, 34057, 34183

DELETE FROM `reference_loot_template` WHERE `entry` = '34111';
INSERT INTO `reference_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES
('34111','46339','0','1','1','1','1'), -- Mimiron's Repeater
('34111','46340','0','1','1','1','1'), -- Adamant Handguards
('34111','46341','0','1','1','1','1'), -- Drape of the Spellweaver
('34111','46342','0','1','1','1','1'), -- Golemheart Longbow
('34111','46343','0','1','1','1','1'), -- Fervor of the Protectorate
('34111','46344','0','1','1','1','1'), -- Iceshear Mantle
('34111','46345','0','1','1','1','1'), -- Bracers of Righteous Reformation
('34111','46346','0','1','1','1','1'), -- Boots of Unsettled Prey
('34111','46347','0','1','1','1','1'), -- Cloak of the Dormant Blaze
('34111','46350','0','1','1','1','1'), -- Pillar of Fortitude
('34111','46351','0','1','1','1','1'); -- Bloodcrush Cudgel


UPDATE `creature_template` SET `lootid` = '34137' WHERE `entry` = '34137';
DELETE FROM `creature_loot_template` WHERE `entry` = '34137';
INSERT INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES
('34137','1','100','1','0','-34109','1'),
('34137','20','0.1','1','0','-600000','1'),
('34137','45912','0.05','1','0','1','1');

UPDATE `creature_template` SET `lootid` = '34267' WHERE `entry` = '34267';
DELETE FROM `creature_loot_template` WHERE `entry` = '34267';
INSERT INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES
('34267','1','100','1','0','-34109','1'),
('34267','20','0.1','1','0','-600000','1'),
('34267','45912','0.05','1','0','1','1');

DELETE FROM `creature_loot_template` WHERE `entry` = '33772';
INSERT INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES
('33772','1','100','1','0','-34112','1'),
('33772','20','0.1','1','0','-600000','1'),
('33772','45912','0.05','1','0','1','1');

DELETE FROM `creature_loot_template` WHERE `entry` = '33528';
INSERT INTO `creature_loot_template` (`entry`, `item`, `ChanceOrQuestChance`, `lootmode`, `groupid`, `mincountOrRef`, `maxcount`) VALUES
('33528','1','100','1','0','-34112','1'),
('33528','20','0.1','1','0','-600000','1'),
('33528','33568','15','1','0','1','1'),
('33528','45912','0.05','1','0','1','1');