-- SmartAI NPC 27315, 27336
UPDATE `creature_template` SET `AIName`='SmartAI' WHERE `entry` IN ('27315','27336');

DELETE FROM `smart_scripts` WHERE `source_type`=0 AND `entryorguid` IN ('27315', '27336');
INSERT INTO `smart_scripts` (`entryorguid`, `source_type`, `id`, `link`, `event_type`, `event_phase_mask`, `event_chance`, `event_flags`, `event_param1`, `event_param2`, `event_param3`, `event_param4`, `action_type`, `action_param1`, `action_param2`, `action_param3`, `action_param4`, `action_param5`, `action_param6`, `target_type`, `target_param1`, `target_param2`, `target_param3`, `target_x`, `target_y`, `target_z`, `target_o`, `comment`) VALUES
('27315','0','0','1','8','0','100','0','48363','0','0','0','28','49774','0','0','0','0','0','1','0','0','0','0','0','0','0','Helpless Wintergarde Villager - on spellhit - Remove Aura'),
('27315','0','1','2','61','0','100','0','0','0','0','0','11','43671','0','0','0','0','0','7','0','0','0','0','0','0','0','Helpless Wintergarde Villager - on spellhit - mount to invoker'),
('27315','0','2','0','61','0','100','0','0','0','0','0','22','2','0','0','0','0','0','1','0','0','0','0','0','0','0','Helpless Wintergarde Villager - on spellhit - set phasemask to 2'),
('27315','0','3','0','1','2','100','1','5000','5000','5000','5000','1','0','0','0','0','0','0','1','0','0','0','0','0','0','0','Helpless Wintergarde Villager - OOC (Phase 2) - Say text 0'),
('27315','0','4','5','23','2','100','1','43671','0','1000','1000','1','1','0','0','0','0','0','1','0','0','0','0','0','0','0','Helpless Wintergarde Villager - On Aura Missing - say text'),
('27315','0','5','0','61','2','100','0','0','0','0','0','41','5000','0','0','0','0','0','1','0','0','0','0','0','0','0','Helpless Wintergarde Villager - On Aura Missing -  despawn after 2 secs'),
('27336','0','0','1','8','0','100','0','48363','0','0','0','28','49774','0','0','0','0','0','1','0','0','0','0','0','0','0','Helpless Wintergarde Villager - on spellhit - Remove Aura'),
('27336','0','1','2','61','0','100','0','0','0','0','0','11','43671','0','0','0','0','0','7','0','0','0','0','0','0','0','Helpless Wintergarde Villager - on spellhit - mount to invoker'),
('27336','0','2','0','61','0','100','0','0','0','0','0','22','2','0','0','0','0','0','1','0','0','0','0','0','0','0','Helpless Wintergarde Villager - on spellhit - set phasemask to 2'),
('27336','0','3','0','1','2','100','1','5000','5000','5000','5000','1','0','0','0','0','0','0','1','0','0','0','0','0','0','0','Helpless Wintergarde Villager - OOC (Phase 2) - Say text 0'),
('27336','0','4','5','23','2','100','1','43671','0','1000','1000','1','1','0','0','0','0','0','1','0','0','0','0','0','0','0','Helpless Wintergarde Villager - On Aura Missing - say text'),
('27336','0','5','0','61','2','100','0','0','0','0','0','41','5000','0','0','0','0','0','1','0','0','0','0','0','0','0','Helpless Wintergarde Villager - On Aura Missing -  despawn after 2 secs');

-- Condition for spell 48397, 48363
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId` IN ('13', '17') AND `SourceEntry` IN ('48397', '48363');
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
('13','1','48363','0','0','31','0','3','27315','0','0','0','0','','Grab target Helpless Wintergarde Villager'),
('13','1','48363','0','1','31','0','3','27336','0','0','0','0','','Grab target Helpless Wintergarde Villager'),
('17','0','48363','0','0','29','0','27315','5','0','0','0','0','','Required NPC to cast Spell'),
('17','0','48363','0','1','29','0','27336','5','0','0','0','0','','Required NPC to cast Spell'),
('17','0','48397','0','0','29','0','27315','5','0','0','0','0','','Required NPC to cast Spell'),
('17','0','48397','0','1','29','0','27336','5','0','0','0','0','','Required NPC to cast Spell');

-- Aura mount 27258
DELETE FROM `creature_template_addon` WHERE `entry`= '27258';
INSERT INTO `creature_template_addon` (`entry`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES
('27258', '0', '0', '33554432', '0', '0', '34873');

-- Update creature_template 27258
UPDATE `creature_template` SET `exp`= '2', `speed_walk`= '1', `speed_run`= '1.142857',`faction`= '35', `spell1`= '48363', `spell2`= '48397', `spell3`= '54170', `VehicleId`= '44', `InhabitType`= '5'  WHERE  `entry`= '27258';

-- Condition mount 27258
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`= '16' AND `SourceEntry`= '27258';
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
('16','0','27258','0','0','23','0','4188','0','0','0','0','0','','Dismount player when not in intended zone'),
('16','0','27258','0','1','23','0','4177','0','0','0','0','0','','Dismount player when not in intended zone'),
('16','0','27258','0','2','23','0','4178','0','0','0','0','0','','Dismount player when not in intended zone');

-- Remove the dropped NPC
DELETE FROM `spell_linked_spell` WHERE `spell_trigger`= '48397';
INSERT INTO `spell_linked_spell` (`spell_trigger`, `spell_effect`, `type`, `comment`) VALUES 
('48397', '-43671', '0', 'remove dropped npc from mount');

-- Update creature_template 27315, 27336
UPDATE `creature_template` SET `minlevel`= '60', `maxlevel`= '70', `unit_flags`= '33536' WHERE  `entry`= '27315';
UPDATE `creature_template` SET `minlevel`= '62', `maxlevel`= '68', `unit_flags`= '33536' WHERE  `entry`= '27336';

-- Spawn NPC 27315, 27336
DELETE FROM `creature` WHERE `id` IN ('27315', '27336');
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES
('75043','27315','571','0','0','1','1','0','0','3768.52','-1289.78','133.653','4.8278','120','5','0','1','0','1','0','0','0'),
('75042','27315','571','0','0','1','1','0','0','3718.5','-1323.4','125.035','1.81862','120','5','0','1','0','1','0','0','0'),
('75041','27315','571','0','0','1','1','0','0','3721.79','-1345.79','133.677','0.287154','120','5','0','1','0','1','0','0','0'),
('75040','27315','571','0','0','1','1','0','0','3625.33','-1260.05','112.612','6.23729','120','5','0','1','0','1','0','0','0'),
('75039','27315','571','0','0','1','1','0','0','3664.3','-1270.02','112.583','5.26854','120','5','0','1','0','1','0','0','0'),
('75038','27315','571','0','0','1','1','0','0','3822.96','-1128.77','120.199','5.97348','120','5','0','1','0','1','0','0','0'),
('75044','27336','571','0','0','1','1','0','0','3707.7','-1138.64','120.226','4.97419','120','0','0','1','0','0','0','0','0'),
('75037','27336','571','0','0','1','1','0','0','3583.05','-1331.91','109.345','6.21278','120','5','0','1','0','1','0','0','0'),
('75031','27336','571','0','0','1','1','0','0','3846.75','-929.04','112.891','4.70838','120','5','0','1','0','1','0','0','0'),
('75034','27336','571','0','0','1','1','0','0','3823.8','-1083.31','119.539','1.96393','120','5','0','1','0','1','0','0','0'),
('75035','27336','571','0','0','1','1','0','0','3817.07','-1048.89','119.909','1.82263','120','5','0','1','0','1','0','0','0'),
('75036','27336','571','0','0','1','1','0','0','3771.59','-1131.89','121.54','5.28835','120','5','0','1','0','1','0','0','0'),
('75033','27336','571','0','0','1','1','0','0','3878.46','-985.889','116.458','5.02889','120','5','0','1','0','1','0','0','0'),
('75032','27336','571','0','0','1','1','0','0','3889.42','-935.48','115.646','1.28458','120','5','0','1','0','1','0','0','0');

-- Update creature_template_addon 27315, 27336
DELETE FROM `creature_template_addon` WHERE `entry` IN ('27315', '27336');
INSERT INTO `creature_template_addon` (`entry`, `path_id`, `mount`, `bytes1`, `bytes2`, `emote`, `auras`) VALUES
('27315','0','0','65536','1','0','48361 49774'),
('27336','0','0','0','1','0','48361 49774');

-- Creature_text NPC 27315, 27336
DELETE FROM `creature_text` WHERE `entry` IN ('27315', '27336');
INSERT INTO `creature_text` (`entry`, `groupid`, `id`, `text`, `type`, `language`, `probability`, `emote`, `duration`, `sound`, `TextRange`, `comment`) VALUES
('27315','0','0','Are you sure you know how to fly this thing? Feels a little wobbly.','12','0','100','0','0','0','0','Helpless Wintergarde Villager'),
('27315','0','1','I don\'t mean to sound ungrateful, but could you fly a little closer to the ground? I hate heights!','12','0','100','0','0','0','0','Helpless Wintergarde Villager'),
('27315','0','2','I picked a bad day to stop drinking!','12','0','100','0','0','0','0','Helpless Wintergarde Villager'),
('27315','0','3','I\'m gettin\' a little woozy... Oooooof...','12','0','100','0','0','0','0','Helpless Wintergarde Villager'),
('27315','0','4','You saved my life! Thanks!','12','0','100','0','0','0','0','Helpless Wintergarde Villager'),
('27315','0','5','You are my guardian angel! Like a white knight you flew in from the heavens and lifted me from the pit of damnation!','12','0','100','0','0','0','0','Helpless Wintergarde Villager'),
('27315','1','0','How can I ever repay you for this, friend?','12','0','100','0','0','0','0','Helpless Wintergarde Villager'),
('27315','1','1','HURRAY!','12','0','100','0','0','0','0','Helpless Wintergarde Villager'),
('27315','1','2','Kindness is not lost with this one, Urik. Thank you, hero!','12','0','100','0','0','0','0','Helpless Wintergarde Villager'),
('27315','1','3','My shop\'s doors will always be open to you, friend.','12','0','100','0','0','0','0','Helpless Wintergarde Villager'),
('27315','1','4','Safe at last! Thank you, stranger!','12','0','100','0','0','0','0','Helpless Wintergarde Villager'),
('27315','1','5','Thanks for your help, hero!','12','0','100','0','0','0','0','Helpless Wintergarde Villager'),
('27315','1','6','We made it! We actually made it!','12','0','100','0','0','0','0','Helpless Wintergarde Villager'),
('27336','0','0','Are you sure you know how to fly this thing? Feels a little wobbly.','12','0','100','0','0','0','0','Helpless Wintergarde Villager'),
('27336','0','1','I don\'t mean to sound ungrateful, but could you fly a little closer to the ground? I hate heights!','12','0','100','0','0','0','0','Helpless Wintergarde Villager'),
('27336','0','2','I picked a bad day to stop drinking!','12','0','100','0','0','0','0','Helpless Wintergarde Villager'),
('27336','0','3','I\'m gettin\' a little woozy... Oooooof...','12','0','100','0','0','0','0','Helpless Wintergarde Villager'),
('27336','0','4','You saved my life! Thanks!','12','0','100','0','0','0','0','Helpless Wintergarde Villager'),
('27336','0','5','You are my guardian angel! Like a white knight you flew in from the heavens and lifted me from the pit of damnation!','12','0','100','0','0','0','0','Helpless Wintergarde Villager'),
('27336','1','0','How can I ever repay you for this, friend?','12','0','100','0','0','0','0','Helpless Wintergarde Villager'),
('27336','1','1','HURRAY!','12','0','100','0','0','0','0','Helpless Wintergarde Villager'),
('27336','1','2','Kindness is not lost with this one, Urik. Thank you, hero!','12','0','100','0','0','0','0','Helpless Wintergarde Villager'),
('27336','1','3','My shop\'s doors will always be open to you, friend.','12','0','100','0','0','0','0','Helpless Wintergarde Villager'),
('27336','1','4','Safe at last! Thank you, stranger!','12','0','100','0','0','0','0','Helpless Wintergarde Villager'),
('27336','1','5','Thanks for your help, hero!','12','0','100','0','0','0','0','Helpless Wintergarde Villager'),
('27336','1','6','We made it! We actually made it!','12','0','100','0','0','0','0','Helpless Wintergarde Villager');