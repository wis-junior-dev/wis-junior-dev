-- Spawn creature 24820
DELETE FROM `creature` WHERE `id` = '24820';
INSERT INTO `creature` (`guid`, `id`, `map`, `zoneId`, `areaId`, `spawnMask`, `phaseMask`, `modelid`, `equipment_id`, `position_x`, `position_y`, `position_z`, `orientation`, `spawntimesecs`, `spawndist`, `currentwaypoint`, `curhealth`, `curmana`, `MovementType`, `npcflag`, `unit_flags`, `dynamicflags`) VALUES
('49220','24820','571','0','0','1','1','0','0','466.754','-5921.27','309.139','0.751123','120','0','0','1','0','0','0','0','0');

-- Conditions
DELETE FROM `conditions` WHERE `SourceTypeOrReferenceId`='13' AND `SourceEntry`='44550';
INSERT INTO `conditions` (`SourceTypeOrReferenceId`, `SourceGroup`, `SourceEntry`, `SourceId`, `ElseGroup`, `ConditionTypeOrReference`, `ConditionTarget`, `ConditionValue1`, `ConditionValue2`, `ConditionValue3`, `NegativeCondition`, `ErrorType`, `ErrorTextId`, `ScriptName`, `Comment`) VALUES
('13','1','44550','0','0','31','0','3','24820','0','0','0','0','','Collect Data target');